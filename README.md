# Start Page

A page that will become my browser's start page, to make it easy to click on certain links, and display details like day, current time, and current weather.

## Live Demo

[projects.gregoryhammond.ca/personalizedstartpage/](https://projects.gregoryhammond.ca/personalizedstartpage/)

## Built With

- Flexbox

## License

[Unlicense](https://unlicense.org/)

## Deployment

To see or use this project on your own computer/server, download the master branch, then open the html file.

If you change any of the folders or file names you will need to change where they are first referenced.

## Lessons Learned

- You can show the current date and time on the page without having any external references, using [Intl.DateTimeFormat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat)

- Showing the weather will require use of an API, I chose [OpenWeatherMap](https://openweathermap.org/api) which has great documentation

- Since I didn't want my OpenWeatherMap API key to be public (I read some horror stories of different API key's being public and ranking up huge charges), I used [Netlify snippet injection](https://www.netlify.com/docs/inject-analytics-snippets) (which I had never used before).

- While you can use a widget to display the weather it may load tons of requests in the background that always clear where they go, and if they are in fact privacy-friendly.

- To have a new line (while using Flexbox) without having to try and give space there is a trick in hr, with making the width 100% and making the height, margin and border set to 0.

- Flex-basis and width are very similar, but [width is supported in more browsers](https://caniuse.com/mdn-css_properties_width) (than [flex-basis](https://caniuse.com/mdn-css_properties_flex-basis)).

- Making the page look good on a small screen required a fair amount of tweaking, as wanted to make sure everything was viewable and usable on that small of a screen.

## Contact

Gregory Hammond - [https://gregoryhammond.ca/](https://gregoryhammond.ca/) - [@devGregory on Twitter](https://twitter.com/devgregory)

## Acknowledgements

[HTML5 Boilerplate](https://github.com/h5bp/html5-boilerplate/blob/master/dist/index.html) - for the template for this site

[Miguel's Bento repo](https://github.com/migueravila/Bento) - which was some of the inspiration for this

[OpenWeatherMap](https://openweathermap.org) - for having an API that has a good number of calls per minute for free

[Minireset.css](https://jgthms.com/minireset.css/)

The many sites and people's content or comment who helped me to create this, they are referenced in the comments on the html and css.